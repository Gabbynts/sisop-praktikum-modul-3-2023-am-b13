#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

int random_num(int min, int max){
    int num = rand() % (max - min + 1) + min;
    return num;
}

int main(){
    printf("Program kalian.c\n");
    int a, b, c;
    a = 4;
    b = 2;
    c = 5;

    int matrix1[a][b], matrix2[b][c], matrix[a][c];
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int[a][c]), IPC_CREAT | 0666);    
    int (*ptr)[c] = (int (*)[c]) shmat(shmid, NULL, 0);
    for(int i=0; i<a; i++){
        for(int j=0; j<b; j++){
            matrix1[i][j] = random_num(1, 5);
        }
    } 
    for(int i=0; i<b; i++){
        for(int j=0; j<c; j++){
            matrix2[i][j] = random_num(1, 4);
        }
    }
    for(int i=0; i<a; i++){
        for(int j=0; j<c; j++){
            int cell=0;
            for(int k=0; k<b; k++){
                cell += matrix1[i][k] * matrix2[k][j];
            }
            matrix[i][j] = cell;
            ptr[i][j] = matrix[i][j];
        }
    }

    printf("Matrix 1\n");
    for(int i=0; i<a; i++){
        for(int j=0; j<b; j++){
            printf("%d ", matrix1[i][j]);
        }
        printf("\n");
    }

    printf("\nMatrix 2\n");
    for(int i=0; i<b; i++){
        for(int j=0; j<c; j++){
            printf("%d ", matrix2[i][j]);
        }
        printf("\n");
    }

    printf("\nMatrix 1 x Matrix 2\n");
    for(int i=0; i<a; i++){
        for(int j=0; j<c; j++){
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    shmdt(ptr);
    return 0;
}
