#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

typedef struct{
    int num;
    unsigned long long f_num;
} Cell_matrix;

void *factorial(void *args){
    Cell_matrix *cells = (Cell_matrix *)args;
    int n = cells->num;
    unsigned long long total = 1;

    for (int i = 1; i <= n; i++) {
        total *= i;
    }

    cells->f_num = total;
    pthread_exit(NULL);
}

int main(){
    printf("\n=========================\n");
    printf("Program cinta.c\n");
    pthread_t threads[20];
    Cell_matrix c_m[20];
    key_t key = 1234;
    key_t key2 = 5678;
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);    
    int (*ptr)[5] = (int (*)[5]) shmat(shmid, NULL, 0);
    int shmid2 = shmget(key2, sizeof(double), IPC_CREAT | 0666);    
    double *multithread_time;
	multithread_time = shmat(shmid2, NULL, 0);

    printf("Matrix\n");
    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            printf("%d ", ptr[i][j]);
        }
        printf("\n");
    }

    clock_t start = clock();
    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            int result;
            c_m[i*5+j].num = ptr[i][j];
            pthread_create(&threads[i*5+j], NULL, factorial, &c_m[i*5+j]);
        }
    }
    printf("Matriks faktorial program cinta.c\n");
    for (int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            pthread_join(threads[i*5+j], NULL);
            printf("%llu ", c_m[i*5+j].f_num);
        }
        printf("\n");
    }
    clock_t end = clock();

    *multithread_time = ((double)(end - start)) / CLOCKS_PER_SEC;
    shmdt(ptr);
    shmdt(multithread_time);
    return 0;
}
