#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

typedef struct{
    int num;
    unsigned long long f_num;
} Cell_matrix;

void factorial(Cell_matrix *c_m, int m[], int n){
    unsigned long long total = 1;
    for (int i = 1; i<=m[n] ; i++) {
        total *= i;
    }
    c_m[n].f_num = total;
}

int main(){
    printf("\n=========================\n");
    printf("Program sisop.c\n");
    int matrix[20];
    Cell_matrix cell_m[20];
    key_t key = 1234;
    key_t key2 = 5678;
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);    
    int (*ptr)[5] = (int (*)[5]) shmat(shmid, NULL, 0);
    int shmid2 = shmget(key2, sizeof(double), IPC_CREAT | 0666);    
    double *multithread_time;
	multithread_time = shmat(shmid2, NULL, 0);

    printf("Matrix\n");
    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            matrix[i*5+j] = ptr[i][j];
            printf("%d ", matrix[i*5+j]);
        }
        printf("\n");
    }

    printf("Waktu dengan multithread : %.5f s\n\n", *multithread_time);
    clock_t start = clock();
    printf("Matrix faktorial program sisop.c\n");
	for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            factorial(cell_m, matrix, i*5+j);
        }
    }
    for (int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            printf("%llu ", cell_m[i*5+j].f_num);
        }
        printf("\n");
    }
    clock_t end = clock();
    double withoutThread_time = ((double)(end - start)) / CLOCKS_PER_SEC;
    printf("\n\nWaktu tanpa multithread : %.5f s\n", withoutThread_time);
    shmdt(ptr);
    shmdt(multithread_time);
    return 0;
}
