#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <json-c/json.h>
#include <semaphore.h>

#define MAX_TEXT_SIZE 512
#define MAX_ACTIVE_USERS 2

char* base64(const char* input);
char* rot13(char* str);
char* hex(char* str);

int compare_strings(const void* a, const void* b);

void decrypt(const char* input_file, const char* output_file);
void print_playlist();

char* strcasestr(const char* haystack, const char* needle);

void play(char* song_name, pid_t user_pid);
void add_song_to_playlist(char* song_name, pid_t user_pid);
void update_playlist();

struct message_buffer {
    long message_type;
    char message_text[MAX_TEXT_SIZE];
    pid_t pid;  
};

int main() {
    key_t key;
    int message_id;
    struct message_buffer message;
    pid_t active_users[MAX_ACTIVE_USERS];
    int active_users_count = 0;

    if ((key = ftok("stream.c", 'B')) == -1) {
        perror("ftok");
        exit(1);
    }

    if ((message_id = msgget(key, 0644 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    //3F
    sem_t *sem;
    if ((sem = sem_open("/thesemaphore", O_CREAT, 0644, MAX_ACTIVE_USERS)) == SEM_FAILED) {
        perror("sem_open");
        exit(1);
    }

    printf("This is Stream: \n");

    while (1) {
    //3A
        if (msgrcv(message_id, &message, sizeof(message) - sizeof(long), 0, 0) == -1) {
            perror("msgrcv");
            exit(1);
        }

        int found_user = 0;
        for (int i = 0; i < active_users_count; i++) {
            if (active_users[i] == message.pid) {
                found_user = 1;
                break;
            }
        }

        if (!found_user && active_users_count == MAX_ACTIVE_USERS) {
            printf("STREAM SYSTEM OVERLOAD: User %d cannot send commands at the moment.\n", message.pid);
            continue;
        }

        if (!found_user) {
            if (sem_wait(sem) == -1) {
                perror("sem_wait");
                exit(1);
            }
            active_users[active_users_count++] = message.pid;
        }

        //
        if (strcmp(message.message_text, "DECRYPT") == 0) {
            decrypt("song-playlist.json", "playlist.txt");
        } 
        //
        else if (strcmp(message.message_text, "LIST") == 0) {
            print_playlist();
            } else if (strcmp(message.message_text, "quit") == 0) {
            int found_user = 0;
            for (int i = 0; i < active_users_count; i++) {
                if (active_users[i] == message.pid) {
                    found_user = 1;

                    for (int j = i; j < active_users_count - 1; j++) {
                        active_users[j] = active_users[j + 1];
                    }
                    active_users_count--;
                    break;
                }
            }

            if (found_user) {
                printf("User %d has quit.\n", message.pid);
            } else {
                printf("User %d is not active.\n", message.pid);
            }
        } 
        //3D
        else if (strncmp(message.message_text, "PLAY \"", 6) == 0) {
            char song_name[MAX_TEXT_SIZE];
            strcpy(song_name, message.message_text + 4);

            play(song_name, message.pid);
        } 
        //3E
        else if (strncmp(message.message_text, "ADD ", 4) == 0) {
            char song_name[MAX_TEXT_SIZE];
            strcpy(song_name, message.message_text + 4);

            add_song_to_playlist(song_name, message.pid); 
        } 
        //3G
        else {
            printf("UNKNOWN COMMAND: %s\n", message.message_text);
        }

        if (!found_user) {
            if (sem_post(sem) == -1) {
                perror("sem_post");
                exit(1);
            }
        }
    }

    sem_close(sem);
    sem_unlink("/thesemaphore");

    return 0;
}

char* rot13(char *str) {
    char c;
    for(int i=0; str[i] != '\0'; i++) {
        c = str[i];
        if(c >= 'A' && c <= 'Z') {
            c = ((c - 'A') + 13) % 26 + 'A';
        } else if(c >= 'a' && c <= 'z') {
            c = ((c - 'a') + 13) % 26 + 'a';
        }
        str[i] = c;
    }
    return str;
}

char* base64(const char* input) {
    const char* BASE64_ALPHABET ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
 
    size_t len = strlen(input);
    size_t padding = 0;
 
    if (input[len-1] == '=') {
        padding++;
        if (input[len-2] == '=') {
            padding++;
        }
    }
 
    size_t outlen = len * 3 / 4 - padding;
 
    char* output = malloc(outlen + 1);
 
    unsigned char buffer[4];
    size_t j = 0;
 
    for (size_t i = 0; i < len; i += 4) {
        buffer[0] = strchr(BASE64_ALPHABET, input[i]) - BASE64_ALPHABET;
        buffer[1] = strchr(BASE64_ALPHABET, input[i+1]) - BASE64_ALPHABET;
        buffer[2] = strchr(BASE64_ALPHABET, input[i+2]) - BASE64_ALPHABET;
        buffer[3] = strchr(BASE64_ALPHABET, input[i+3]) - BASE64_ALPHABET;
 
        output[j++] = (char) ((buffer[0] << 2) | (buffer[1] >> 4));
        if (j < outlen) {
            output[j++] = (char) ((buffer[1] << 4) | (buffer[2] >> 2));
        }
        if (j < outlen) {
            output[j++] = (char) ((buffer[2] << 6) | buffer[3]);
        }
    }
 
    output[outlen] = '\0';
 
    return output;
}
 
char* hex(char *str) {
    char hexChar[3];
    int i, len = strlen(str);
    char *newStr = malloc(len/2 + 1);

    for(i=0; i<len-1; i+=2) {
        sprintf(hexChar, "%c%c", str[i], str[i+1]);
        int value = (int)strtol(hexChar, NULL, 16);
        newStr[i/2] = value;
    }
    newStr[i/2] = '\0';
 
    return newStr;
}
 
int compare_strings(const void* a, const void* b) {
    const char** string_a = (const char**) a;
    const char** string_b = (const char**) b;
 
    return strcasecmp(*string_a, *string_b);
}

void decrypt(const char *input_file, const char *output_file) {
    FILE *fp;
    FILE *out_fp;
    char buffer[100000];

    struct json_object *parsed_json;
    struct json_object *method;
    struct json_object *song;

    size_t n_objects;
    size_t i;

    fp = fopen(input_file, "r");
    fread(buffer, 100000, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);

    if (parsed_json == NULL) {
        fprintf(stderr, "Error: failed to parse JSON\n");
        return;
    }

    out_fp = fopen(output_file, "w");

    if (json_object_is_type(parsed_json, json_type_array)) {
        n_objects = json_object_array_length(parsed_json);

        char **decrypted_songs = (char **) malloc(n_objects * sizeof(char *));

        for (i = 0; i < n_objects; i++) {
            struct json_object *obj = json_object_array_get_idx(parsed_json, i);

            json_object_object_get_ex(obj, "method", &method);
            json_object_object_get_ex(obj, "song", &song);

            char *decrypted_song;
            if (strcmp(json_object_get_string(method), "rot13") == 0) {
                decrypted_song = rot13(strdup(json_object_get_string(song)));
            } else if (strcmp(json_object_get_string(method), "base64") == 0) {
                decrypted_song = base64(strdup(json_object_get_string(song)));
            } else if (strcmp(json_object_get_string(method), "hex") == 0) {
                decrypted_song = hex(strdup(json_object_get_string(song)));
            } else {
                decrypted_song = strdup(json_object_get_string(song));
            }

            decrypted_songs[i] = decrypted_song;
        }

        qsort(decrypted_songs, n_objects, sizeof(char *), compare_strings);

        for (i = 0; i < n_objects; i++) {
            fprintf(out_fp, "%s\n", decrypted_songs[i]);
            free(decrypted_songs[i]);
        }

        free(decrypted_songs);
    }

    fclose(out_fp);
}

void print_playlist() {
    FILE* playlist_fp = fopen("playlist.txt", "r");
    if (playlist_fp == NULL) {
        perror("Error: failed to open playlist file");
        return;
    }

    char line[MAX_TEXT_SIZE];
    while (fgets(line, sizeof(line), playlist_fp)) {
        printf("%s", line);
    }

    fclose(playlist_fp);
}


char *strcasestr(const char *haystack, const char *needle)
{
    size_t needle_len = strlen(needle);

    if (needle_len == 0) {
        return (char *)haystack;
    }

    for (; *haystack; ++haystack) {
        if (strncasecmp(haystack, needle, needle_len) == 0) {
            return (char *)haystack;
        }
    }

    return NULL;
}

void play(char *song_name, pid_t user_pid) {
    char *start_quote = strchr(song_name, '"');
    if (start_quote == NULL) {
        printf("Error: invalid song name format\n");
        return;
    }
    char *end_quote = strchr(start_quote + 1, '"');
    if (end_quote == NULL) {
        printf("Error: invalid song name format\n");
        return;
    }
    strncpy(song_name, start_quote + 1, end_quote - start_quote - 1);
    song_name[end_quote - start_quote - 1] = '\0';

    FILE *playlist_fp = fopen("playlist.txt", "r");
    if (playlist_fp == NULL) {
        perror("Error: failed to open playlist file");
        return;
    }

    char line[MAX_TEXT_SIZE];
    int n_songs = 0;

    while (fgets(line, sizeof(line), playlist_fp)) {
        if (strcasestr(line, song_name)) {
            n_songs++;
        }
    }

    fclose(playlist_fp);

    if (n_songs == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", song_name);
    } else if (n_songs == 1) {
        playlist_fp = fopen("playlist.txt", "r");
        if (playlist_fp == NULL) {
            perror("Error: failed to open playlist file");
            return;
        }
        while (fgets(line, sizeof(line), playlist_fp)) {
            if (strcasestr(line, song_name)) {
            printf("USER %d PLAYING %s", (int)user_pid, line);
                break;
            }
        }
        fclose(playlist_fp);
    } else {
        playlist_fp = fopen("playlist.txt", "r");
        if (playlist_fp == NULL) {
            perror("Error: failed to open playlist file");
            return;
        }
        int song_number = 1;
        while (fgets(line, sizeof(line), playlist_fp)) {
            if (strcasestr(line, song_name)) {
                printf("%d. %s", song_number, line);
                song_number++;
            }
        }
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\"\n", n_songs, song_name);
        fclose(playlist_fp);
    }
}

void add_song_to_playlist(char *song_name, pid_t user_pid) {
    FILE *playlist_fp = fopen("playlist.txt", "r+");
    if (playlist_fp == NULL) {
        perror("Error: failed to open playlist file");
        return;
    }

    char line[MAX_TEXT_SIZE];
    while (fgets(line, sizeof(line), playlist_fp)) {
        if (strcasestr(line, song_name)) {
            printf("SONG ALREADY ON PLAYLIST\n");
            fclose(playlist_fp);
            return;
        }
    }

    fseek(playlist_fp, 0, SEEK_END);
    fprintf(playlist_fp, "%s\n", song_name);

    fclose(playlist_fp);

    printf("USER %d ADD %s\n", (int)user_pid, song_name);

    update_playlist();
}

void update_playlist() {
    const char* filename = "playlist.txt";
    const int MAX_LINES = 1500;
    char* lines[MAX_LINES];
    int num_lines = 0;

    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Error: Could not open file %s\n", filename);
        return;
    }
    char buffer[100000];
    while (fgets(buffer, sizeof(buffer), file) != NULL) {
        buffer[strcspn(buffer, "\r\n")] = '\0'; 
        lines[num_lines] = strdup(buffer); 
        num_lines++;
    }
    fclose(file);

    qsort(lines, num_lines, sizeof(char*), compare_strings);

    file = fopen(filename, "w");
    if (file == NULL) {
        printf("Error: Could not open file %s\n", filename);
        return;
    }
    for (int i = 0; i < num_lines; i++) {
        fprintf(file, "%s\n", lines[i]);
        free(lines[i]); 
    }
    fclose(file);
}

