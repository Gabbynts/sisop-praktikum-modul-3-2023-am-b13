#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAX_TEXT_SIZE 512

//3A
struct message_buffer {
    long type;
    char message[MAX_TEXT_SIZE];
    pid_t user_pid;
};

int main() {
    key_t key;
    int message_id;
    struct message_buffer message;

    if ((key = ftok("stream.c", 'B')) == -1) {
        perror("ftok");
        exit(1);
    }

    if ((message_id = msgget(key, 0644)) == -1) {
        perror("msgget");
        exit(1);
    }

    char input[MAX_TEXT_SIZE];
    while (1) {
        printf("Type here : ");
        fgets(input, MAX_TEXT_SIZE, stdin);
        input[strcspn(input, "\n")] = '\0'; 
        if (strncmp(input, "ADD ", 3) == 0 || strncmp(input, "DECRYPT", 7) == 0 || strncmp(input, "LIST", 4) == 0 || strncmp(input, "PLAY ", 4) == 0 ) {
            pid_t user_pid = getpid();
            message.user_pid = user_pid;
            strncpy(message.message, input, MAX_TEXT_SIZE);

        } else {
            message.user_pid = 0; 
            strcpy(message.message, input);
        }

        message.type = 1;

        if (msgsnd(message_id, &message, sizeof(message) - sizeof(long), 0) == -1) {
            perror("msgsnd");
            exit(1);
        }
    }

    return 0;
}
