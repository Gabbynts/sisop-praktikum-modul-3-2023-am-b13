#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int main() {
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); 
  }

  if (child_id == 0) {

    char *argv[] = {"wget", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", "-O", "hehe.zip", "-q", NULL};
    execv("/usr/bin/wget", argv);
  } else {
    while ((wait(&status)) > 0);
    char *arg[] = {"unzip", "-q", "hehe.zip", NULL};
    execv("/usr/bin/unzip", arg);
  }
}