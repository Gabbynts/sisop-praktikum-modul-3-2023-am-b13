#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <time.h>
#include <ctype.h>

#define MAX_LENGTH 100
#define MAX_PATH_LENGTH 1024

#define ext_file "./extensions.txt"
#define FILES "./files"

char extensions[MAX_PATH_LENGTH][10];
char log_time[MAX_LENGTH];
char logname[MAX_LENGTH];
char log_name[MAX_LENGTH];
char filename[MAX_PATH_LENGTH];

int ext_count = 0;
int max_values = 0;
int ext_dir[MAX_PATH_LENGTH];

void *process_extension();
void *read_max();
void *move_files();
void *count_extension();

int main() {
    pthread_t threads;

    process_extension();

    pthread_create(&threads, NULL, read_max, NULL); 
    pthread_join(threads, NULL);

    pthread_create(&threads, NULL, move_files, NULL);
    pthread_join(threads,NULL);

    pthread_create(&threads, NULL, count_extension, NULL);
    pthread_join(threads,NULL);

    system("rm temp_file.txt");

    return 0;
}

const char *get_ext(char *dir){
    const char *name = strrchr(dir, '.');
    if(!name || name == dir)
        return "";
    return name + 1;
}

int comp_ext(char ext[]) {
    for(int i=0; i<ext_count;i++){
        if(strcmp(ext, extensions[i]) == 0)
            return i;
    }
    return -1;
}

char get_log_time(char *time_str) {
    time_t rawtime;
    struct tm * timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(time_str, 100, "%d-%m-%Y %H:%M:%S", timeinfo);
}

void* process_extension() { 
    FILE *filePointer = fopen(ext_file, "r");

    if (filePointer == NULL) {
        printf("Gagal membuka file!");
        exit(EXIT_FAILURE);
    }

    char buffer[MAX_PATH_LENGTH];
    char **lines = NULL;
    int index=0;

    while (fgets(buffer, MAX_PATH_LENGTH, filePointer)) {
        lines = realloc(lines, sizeof(char*) * (ext_count + 1));
        lines[ext_count] = malloc(sizeof(char) * (strlen(buffer) + 1));

        strncpy(lines[index], buffer, strlen(buffer) - 2);
        ext_dir[index] = 0;

        strcpy(extensions[index], lines[index]);
        index++;
        ext_count++;
    }
    fclose(filePointer);

    chdir("./");

    strcpy(logname, "./");
    strcat(logname, "/log");

    strcpy(log_name, logname);
    strcat(log_name, ".txt");

    sprintf(filename, "categorized");
    mkdir(filename, 0777);

    filePointer = fopen(log_name, "a+");

    get_log_time(log_time);
    fprintf(filePointer, "%s MADE %s\n", log_time, filename);

    fclose(filePointer);

    for (int i = 0; i < ext_count; i++) {

        sprintf(filename, "categorized/%s", extensions[i]);
        mkdir(filename, 0777);

        filePointer = fopen(log_name, "a+");
        get_log_time(log_time);
        fprintf(filePointer, "%s MADE %s\n", log_time, filename);

        fclose(filePointer);

    }
    sprintf(filename, "categorized/other");
    mkdir(filename, 0777);

    filePointer = fopen(log_name, "a+");
    get_log_time(log_time);
    fprintf(filePointer, "%s MADE %s\n", log_time, filename);

    fclose(filePointer);
}

void *scan_files(void *arg) {
    DIR *dir;
    struct dirent *ent;
    char path[MAX_PATH_LENGTH];
    char cmd[9000];

    if ((dir = opendir((char*) arg)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_type == DT_DIR) {
                if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {  // directory  
                    continue;        
                } 
                sprintf(path, "%s/%s", (char*) arg, ent->d_name);

                get_log_time(log_time);
                sprintf(cmd, "echo '%s ACCESSED %s' >> %s", log_time, path, log_name);
                system(cmd);

                pthread_t tid;
                pthread_create(&tid, NULL, scan_files, (void*) path);
                pthread_join(tid, NULL);

            }
            else {
                sprintf(cmd, "echo '%s/%s' >> temp_file.txt", (char*) arg,ent->d_name);
                system(cmd);    
            }
        }
        closedir(dir);
    }
}

void *read_max() {
    FILE *filePointer ;
    char line[255];
    int i = 0;

    filePointer = fopen("./max.txt", "r");

    if (filePointer == NULL) {
        printf("Gagal membuka file!");
        exit(EXIT_FAILURE);
    }

    while (fgets(line, sizeof(line), filePointer) ) {
        max_values = atoi(line);
    }
    fclose(filePointer);

    scan_files(FILES);
}

void *move_files() {
    FILE *filePointer;
    char cmd[9000];
    char ext[MAX_PATH_LENGTH];
    char line[MAX_PATH_LENGTH];
    char buffer[5000];

    filePointer = fopen("./temp_file.txt", "r");

    while(fgets(line, MAX_PATH_LENGTH, filePointer)) {
        line[strlen(line)-1] = '\0';

        sprintf(ext, "%s", get_ext(line));

        for(int i = 0; ext[i]; i++){
            ext[i] = tolower(ext[i]);
        }

        if (comp_ext(ext) == -1) {
            sprintf(cmd, "cp '%s' ./categorized/other", line);
            system(cmd);

            get_log_time(log_time);
            sprintf(cmd, "echo '%s MOVED other file : %s > ./categorized/other' >> %s", log_time, line, log_name);
            system(cmd);
        }
        else { 
            if (ext_dir[comp_ext(ext)] < max_values) {
                sprintf(cmd, "cp '%s' ./categorized/%s", line, ext);
                system(cmd);

                ext_dir[comp_ext(ext)]++;

                get_log_time(log_time);
                sprintf(cmd, "echo '%s MOVED %s file : %s > ./categorized/%s' >> %s", log_time, ext, line, ext, log_name);
                system(cmd);
            }
            else {
                int num_ext = ext_dir[comp_ext(ext)] / max_values + 1;
                sprintf(buffer, "./categorized/%s (%d)", ext, num_ext);

                if(access(buffer, F_OK) != 0){
                    get_log_time(log_time);
                    sprintf(cmd, "echo '%s MADE %s' >> %s", log_time, buffer, "./log.txt");
                    system(cmd);
                }

                mkdir(buffer, 0777);

                sprintf(cmd, "cp '%s' './categorized/%s (%d)'", line, ext, num_ext);
                system(cmd);

                ext_dir[comp_ext(ext)]++;

                get_log_time(log_time);
                sprintf(cmd, "echo '%s MOVED %s file : %s > .%s' >> %s", log_time, ext, line, buffer, log_name);
                system(cmd);
            }
        }
    }
}

void *count_extension() {
    char cmd[9000];
    strcpy(extensions[ext_count], "other");
    DIR *dir;
    struct dirent *ent;
    dir = opendir("./categorized/other");

    get_log_time(log_time);
    sprintf(cmd, "echo '%s ACCESSED ./categorized/other' >> %s", log_time, log_name);
    system(cmd);

    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_type == DT_REG) {
            ext_dir[ext_count]++;
        }
    }

    closedir(dir);
    ext_count++;

    for(int i=0;i<ext_count-1; i++){
        for(int j=0; j<ext_count-i-1;j++){
            if(ext_dir[j] > ext_dir[j+1]){
                int temp;
                temp = ext_dir[j];
                ext_dir[j] = ext_dir[j+1];
                ext_dir[j+1] = temp;

                strcpy(filename, extensions[j]);
                strcpy(extensions[j], extensions[j+1]);
                strcpy(extensions[j+1], filename);
            }
        }
    }
    
    for(int i=0; i<ext_count; i++){
        printf("%s : %d\n", extensions[i], ext_dir[i]);
    }
}
