#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <time.h>
#include <ctype.h>

#define MAX_PATH_LENGTH 1024

char dirpath[MAX_PATH_LENGTH][MAX_PATH_LENGTH];
char extpath[MAX_PATH_LENGTH][MAX_PATH_LENGTH];
char filename[MAX_PATH_LENGTH];

int dir_total[MAX_PATH_LENGTH];
int ext_total[MAX_PATH_LENGTH];

int idxdir = 0;
int idxext = 0;

int accessed();
void print_dir_total();
void print_ext_total();

int main() {
    chdir("./");
    printf("Number of ACCESSED: %d\n\n", accessed());

    printf("List of Directories :\n");
    print_dir_total();
    
    printf("\nTotal File of each Extensions : \n");
    print_ext_total();
    
    return 0;
}

int accessed() {
    FILE *filePointer;
    char line[MAX_PATH_LENGTH];
    int access = 0;

    filePointer = fopen("log.txt", "r");
    if (filePointer == NULL) {
        printf("Gagal membuka file!");
        exit(EXIT_FAILURE);
    }

    while (fgets(line, MAX_PATH_LENGTH, filePointer)) {
        if (strstr(line, "ACCESSED") != NULL) {
            access++;
        }
    }

    return access;
    fclose(filePointer);
}


void print_dir_total() {
    FILE *filePointer;
    char line[MAX_PATH_LENGTH];

    filePointer = fopen("log.txt", "r");

    if (filePointer == NULL) {
        printf("Gagal membuka file!");
        exit(EXIT_FAILURE);
    }

    while (fgets(line, MAX_PATH_LENGTH, filePointer)) {
        line[strlen(line)-1] = '\0';

        if (strstr(line, "MADE") != NULL) {
            char *k = strrchr(line, '/');

            if (k != NULL) {
                dir_total[idxdir] = 0;
                strcpy(dirpath[idxdir++], k+1);
            }
        }

        if (strstr(line, "MOVED") != NULL) {
            char *k = strrchr(line, '/');

            if (k != NULL) {
                for (int i=0 ; i<idxdir ; i++) {
                    if (strcmp(dirpath[i], k+1) == 0) {
                        dir_total[i]++;
                    }
                }
            }
        }
        
    }
    
    for(int i=0;i<idxdir; i++){
        for(int j=0; j<idxdir-1;j++){
            if(dir_total[j] > dir_total[j+1]){
                char buffer[MAX_PATH_LENGTH];
                strcpy(buffer, dirpath[j]);
                strcpy(dirpath[j], dirpath[j+1]);
                strcpy(dirpath[j+1], buffer);

                int temp;
                temp = dir_total[j];
                dir_total[j] = dir_total[j+1];
                dir_total[j+1] = temp;
            }
        }
    }

    for(int i=0; i<idxdir; i++){
        printf("%s : %d\n", dirpath[i], dir_total[i]);
    }

    fclose(filePointer);
}

void print_ext_total() {
    FILE *filePointer;
    char line[MAX_PATH_LENGTH];

    filePointer = fopen("extensions.txt", "r");
    if (filePointer == NULL) {
        printf("Gagal membuka file!");
        exit(EXIT_FAILURE);
    }

    while (fgets(line, MAX_PATH_LENGTH, filePointer)) {
        line[strlen(line)-2] = '\0';
        strcpy(extpath[idxext], line);
        ext_total[idxext] = 0;
        idxext++;
    }
    fclose(filePointer);

    strcpy(extpath[idxext], "other");
    ext_total[idxext]=0;
    idxext++;

    filePointer = fopen("log.txt", "r"); 
    if (filePointer == NULL) {
        printf("Gagal membuka file!");
        exit(EXIT_FAILURE);
    }

    while(fgets(line, MAX_PATH_LENGTH, filePointer)){

        if (strstr(line, "MOVED ") != NULL) {
            line[strlen(line)-1] = '\0';
            char *start = strstr(line, "MOVED ");

            char *en = strstr(start, " file :");
            
            strncpy(filename, start + strlen("MOVED "), en - start - strlen("MOVED "));
            filename[en - start - strlen("MOVED ")] = '\0';

            for (int i=0 ; i<idxdir ; i++) {
                if (strcmp(extpath[i], filename) == 0) {
                    ext_total[i]++;
                }
            }
        }


    }
    fclose(filePointer);

    for(int i=0;i<idxext; i++){
        for(int j=0; j<idxext-1;j++){
            if(ext_total[j] > ext_total[j+1]){
                char buffer[MAX_PATH_LENGTH];
                strcpy(buffer, extpath[j]);
                strcpy(extpath[j], extpath[j+1]);
                strcpy(extpath[j+1], buffer);

                int temp;
                temp = ext_total[j];
                ext_total[j] = ext_total[j+1];
                ext_total[j+1] = temp;
            }
        }
    }

    for(int i=0; i<idxext; i++){
        printf("%s : %d\n", extpath[i], ext_total[i]);
    }
}
