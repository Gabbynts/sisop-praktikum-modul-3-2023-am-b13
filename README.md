# sisop-praktikum-modul-3-2023-AM-B13

## Praktikum modul 3 

### Kelompok B-13 dengan anggota:
| **No** | **Nama** | **NRP** | 
| ------------- | ------------- | --------- |
| 1 | Gabriella Natasya Br Ginting  | 5025211081 | 
| 2 | Muhammad Zikri Ramadhan | 5025211085 |
| 3 | Sandhika Surya Ardyanto | 5025211022 |

### Daftar Isi
- [Soal 1](#soal-1)
    - [a. Kirim hasil perhitungan frekuensi tiap huruf ke child process](#perintah-1-a)
    - [b. Kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf](#perintah-1-b)
    - [c. Kirim kode Huffman ke program dekompresi menggunakan pipe](#perintah-1-c)
    - [d. Baca kode Huffman dan lakukan dekompresi](#perintah-1-d)
    - [e. Tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres](#perintah-1-e)
- [Soal 2](#soal-2)
    - [a. Tampilkan matriks hasil perkalian ke layar](#perintah-2-a) 
    - [b. Tampilkan hasil matriks pengambilan variabel hasil perkalian matriks ke layar](#perintah-2-b)
    - [c. Tampilkan hasil nilai faktorial ke layar dengan format seperti matriks](#perintah-2-c)
    - [d. Dokumentasikan perbandingan program dengan multithread dengan yang tidak](#perintah-2-d)
- [Soal 3](#soal-3)
    - [a. Membuat sistem stream](#perintah-3-a)
    - [b. Melakukan decrypt](#perintah-3-b)
    - [c. Menampilkan daftar lagu yang telah di-decrypt](#perintah-3-c)
    - [d. Mengirimkan perintah PLAY <SONG>](#perintah-3-d)
    - [e. Menambahkan lagu ke dalam playlist](#perintah-3-e)
    - [f. Output-kan "STREAM SYSTEM OVERLOAD" pada sistem](#perintah-3-f)
    - [g. Sistem akan menampilkan "UNKNOWN COMMAND".](#perintah-3-g)
- [Soal 4](#soal-4)
    - [a. Download dan unzip file](#perintah-4-a)
    - [b. Mengumpulkan (move / copy) file sesuai extension-nya](#perintah-4-b)
    - [c. Output-kan pada terminal banyaknya file tiap extension terurut](#perintah-4-c)
    - [d. Akses folder, sub-folder, dan semua folder menggunakan multithreading](#perintah-4-d)
    - [e. Membuat log setiap pengaksesan folder, pemindahan file, pembuatan folder](#perintah-4-e)
    - [f. Mengekstrak informasi dari log.txt](#perintah-4-f)
    
## Soal 1
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 

(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

- ### Perintah 1-a
Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

```
char *filename = "file.txt";
FILE *file = fopen(filename, "r");

int frek[100];
for (int i=0; i<26; i++) frek[i] = 0;
    
char hrf;
while ((hrf = fgetc(file)) != EOF){
    if (65<=hrf<=90 || 97<=hrf<=122){
        hrf = toupper(hrf);
        frek[hrf-65]++;
    }
}
fclose(file);
```
Pada bagian ini, program akan membuka dan membaca file. Selama membaca isi file, program akan mencek apakah karakter yang dibaca merupakan huruf atau bukan. Jika karakter yang dibaca adalah huruf, frekuensi kemunculan huruf tersebut akan ditambah satu pada array frek[]

- ### Perintah 1-b
Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

Pada bagian ini, akan ada 2 proses, pertama seluruh frekuensi huruf akan dimasukkan ke dalam priority queue, lalu tiap node akan dimasukkan ke dalam huffman tree

- Priority Queue

```
typedef struct pqueueNode_t{
	char huruf;
    int data;
    struct pqueueNode_t *next;
} PQueueNode;

typedef struct pqueue_t{
    PQueueNode *_top;
    unsigned _size;
} PriorityQueue;

void pqueue_init(PriorityQueue *pqueue){
    pqueue->_top = NULL;
    pqueue->_size = 0;
}

bool pqueue_isEmpty(PriorityQueue *pqueue){
    return (pqueue->_top == NULL);
}

void pqueue_push(PriorityQueue *pqueue, int value, char huruf){
    PQueueNode *temp = pqueue->_top;
    PQueueNode *newNode = (PQueueNode*) malloc (sizeof(PQueueNode));
    newNode->huruf = huruf;
    newNode->data = value;
    newNode->next = NULL;

    if (pqueue_isEmpty(pqueue)){
        pqueue->_top = newNode;
        return;
    }

    if (value < pqueue->_top->data){
        newNode->next = pqueue->_top;
        pqueue->_top = newNode;
    }
    else{
        while (temp->next != NULL && temp->next->data < value) temp = temp->next;
        while (temp->next != NULL && temp->next->huruf < huruf){
        	if (temp->next->data > value) break;
			temp = temp->next;
		}
        newNode->next = temp->next;
        temp->next = newNode;
    }
}
```

Pada priority queue di atas, node diurut berdassarkan frekunsi terkecil ke terbesar. Jika beberapa huruf memiliki frekuensi yang sama, akan diurut dari leksikografis terkecil

```
for (int i=0; i<26; i++){
    printf("%c %d\n", i+65, frek[i]);
    char temp = i+65;
    if (frek[i] > 0) pqueue_push(&daftar, frek[i], temp);
}
```
Sambil mencetak frekuensi kemunculan semua huruf (termasuk huruf yang tidak muncul), program akan memasukkan huruf dan frekuensi nya ke dalam priority queue jika frekuensi huruf tersebut lebih dari 0.

- Huffman Tree

```
typedef struct tree_node{
    char item;
    int freq;
    struct tree_node *left, *right;
} node_tree;

typedef struct tree__{
    node_tree *_root;
} tree__;

void init_huffman(tree__ *tree){
	tree->_root = NULL;
}

bool isEmpty_huffman(tree__ *tree){
	return (tree->_root == NULL);
}

void insert_to_huffman(tree__ *tree, int value, char huruf){
    
    node_tree *newNode = (node_tree*) malloc (sizeof(node_tree));
    newNode->freq = value;
    newNode->item = huruf;
    newNode->left = NULL;
    newNode->right = NULL;
    
    if (isEmpty_huffman(tree)){
    	tree->_root = newNode;
	}
	else{
	    if (newNode->freq < tree->_root->freq){
	    	node_tree *newRoot = (node_tree*) malloc (sizeof(node_tree));
	    	
	    	newRoot->freq = newNode->freq + tree->_root->freq;
    		newRoot->left = newNode;
    		newRoot->right = tree->_root;
    	
    		tree->_root = newRoot;
		}
		else{
			node_tree *newRoot = (node_tree*) malloc (sizeof(node_tree));
    	
    		newRoot->freq = newNode->freq + tree->_root->freq;
    		newRoot->left = tree->_root;
    		newRoot->right = newNode;
    	
    		tree->_root = newRoot;
    	}
	}
}
```
Pada bagian ini, akan dibuat huffman tree. Dimana tiap 2 node terkecil dari priority queue akan dibandingkan, yang lebih kecil akan menjadi left child, dan yang lebih besar menjadi right child. Root akan berisi jumlah frekuensi dari kedua childnya

- ### Perintah 1-c
Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.

```
...
```

- ### Perintah 1-d
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 

```
...
```

- ### Perintah 1-e
Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

```
...
```

## Soal 2
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

- ### Perintah 2-a
Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

```
printf("Program kalian.c\n");
int a, b, c;
a = 4;
b = 2;
c = 5;

int matrix1[a][b], matrix2[b][c], matrix[a][c];
```
- a = 4 merupakan baris matriks 1. Perkalian matriks 1 dan matriks 2 dimana jumlah kolom matriks 1 dan jumlah baris matriks 2 harus sama sehingga b = 2.
- matrix1[a][b] merupakan matriks dengan ordo 4 x 2. matrix2[b][c] merupakan matriks dengan ordo 2 x 5. matrix[a][c] merupakan matriks hasil dari matrix1 x matrix2 sehingga memiliki ordo 4 x 5. 
---
```
key_t key = 1234;
int shmid = shmget(key, sizeof(int[a][c]), IPC_CREAT | 0666);
int (*ptr)[c] = (int (*)[c]) shmat(shmid, NULL, 0);
```
- key berfungsi sebagai kunci numerik dalam sistem operasi untuk mengidentifikasi segmen shared memory
- shmid berfungsi sebagai identifier untuk mengakses segmen shared memory yang telah dibuat
- shmget berfungsi untuk memmbuat atau mendapatkan segmen shared memory
- shmat berfungsi untuk menghubungkan segmen shared memory ke alamat virtual suatu proses
- ptr sebagai array untuk menyimpan array matrix pada segmen shared memory dengan key = 1234
```
shmdt(ptr);
shmdt(multithread_time);
```
- shmdt berfungsi untuk melepaskan koneksi segmen shared memory
---
```
int random_num(int min, int max){
    int num = rand() % (max - min + 1) + min;
    return num;
}    
```
- Fungsi random_num berfungsi untuk menghasilkan angka acak dengan interval tertentu untuk mengisi sel pada matriks sesuai ketentuan setiap matriks
- rand() untuk menghasilkan angka acak. Hasil dari rand() dioperasikan dengan mod berdasarkan interval antara batas atas (max) dan batas bawah (min). Setelah mendapat interval dari angka acak maka ditambah dengan min karena hasil dari mod dimulai dari 0
```
for(int i=0; i<a; i++){
    for(int j=0; j<b; j++){
        matrix1[i][j] = random_num(1, 5);
    }
}
```
- Mengisi sel pada matriks 1 pada array matrix1 dengan angka acak dari 1 - 5 menggunakan fungsi random_num. Variabel i sebagai baris dan j sebgai kolom
```
    for(int i=0; i<b; i++){
        for(int j=0; j<c; j++){
            matrix2[i][j] = random_num(1, 4);
        }
    }
```
- Mengisi sel pada matriks 2 pada array matrix2 dengan angka acak dari 1 - 4 menggunakan fungsi random_num. Variabel i sebagai baris dan j sebgai kolom
---
```
for(int i=0; i<a; i++){
    for(int j=0; j<c; j++){
        int cell=0;
        for(int k=0; k<b; k++){
            cell += matrix1[i][k] * matrix2[k][j];
        }
        matrix[i][j] = cell;
        ptr[i][j] = matrix[i][j];
    }
}
```
- Berfungsi untuk perkalian matriks 1 dan matriks 2
- Variabel i sebagai baris, j sebagai kolom, dan k sebagai indeks kolom matriks 1 dan baris matriks 2 yang dikalikan
- Variabel cell diinisialisasi dengan 0 untuk total penambahan setiap sel dari perkalian matriks 1 dan matriks 2
- Hasil dari cell disimpan pada array matrix dan ptr. ptr sebagai array dari matrix pada shared memory yang diakses program lain
---
```
printf("Matrix 1\n");
for(int i=0; i<a; i++){
    for(int j=0; j<b; j++){
        printf("%d ", matrix1[i][j]);
    }
    printf("\n");
}
```
- Berfungsi untuk menampilkan matriks 1
- Variabel i sebagai baris dan j sebagai kolom
---
```
    printf("\nMatrix 2\n");
    for(int i=0; i<b; i++){
        for(int j=0; j<c; j++){
            printf("%d ", matrix2[i][j]);
        }
        printf("\n");
    }
```
- Berfungsi untuk menampilkan matriks 2
- Variabel i sebagai baris dan j sebagai kolom
---
```
    printf("\nMatrix 1 x Matrix 2\n");
    for(int i=0; i<a; i++){
        for(int j=0; j<c; j++){
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
```
- Berfungsi untuk menampilkan matriks hasil perkalian matriks 1 dan matriks 2
- Variabel i sebagai baris dan j sebagai kolom
---

- ### Perintah 2-b
Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar.

 ```
    key_t key = 1234;
    key_t key2 = 5678;
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);    
    int (*ptr)[5] = (int (*)[5]) shmat(shmid, NULL, 0);
    int shmid2 = shmget(key2, sizeof(double), IPC_CREAT | 0666);    
    double *multithread_time;
	multithread_time = shmat(shmid2, NULL, 0);
  ```
  - key berfungsi sebagai kunci numerik dalam sistem operasi untuk mengidentifikasi segmen shared memory
  - shmid berfungsi sebagai identifier untuk mengakses segmen shared memory yang telah dibuat
  - shmget berfungsi untuk memmbuat atau mendapatkan segmen shared memory
  - shmat berfungsi untuk menghubungkan segmen shared memory ke alamat virtual suatu proses
  - ptr sebagai array untuk menyimpan array matrix pada segmen shared memory dengan key = 1234
  - multithread_time untuk menyimpan waktu yang dibutuhkan selama menjalankan thread. multithread_time berada pada segmen shared memory dengan key = 5678
  ```
  shmdt(ptr);
  shmdt(multithread_time);
  ```
  - shmdt berfungsi untuk melepaskan koneksi segmen shared memory
  ---
  ```
    printf("Matrix\n");
    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            printf("%d ", ptr[i][j]);
        }
        printf("\n");
    }
  ```
  - Berfungsi untuk menampilkan matrix hasil perkalian pada segmen shared memory
  ---

- ### Perintah 2-c
Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

```
    typedef struct{
        int num;
        unsigned long long f_num;
    } Cell_matrix;
  ```
  - struct Cell_matrix berfungsi untuk menyimpan angka yang akan difaktorialkan (num) dan angka hasil faktorial (f_num)
  - struct dibutuhkan karena fungsi faktorial digunakan pada thread
  ---
```
    void *factorial(void *args){
        Cell_matrix *cells = (Cell_matrix *)args;
        int n = cells->num;
        unsigned long long total = 1;

        for (int i = 1; i <= n; i++) {
            total *= i;
        }

        cells->f_num = total;
        pthread_exit(NULL);
    }
```
- Fungsi *factorial untuk mendapatkan hasil faktorial dari angka setiap sel pada matriks di segmen shared memory dengan key = 1234
- n sebagai angka yang akan difaktorialkan
- total diinisialisasi dengan 1 untuk perkalian pada looping for. Dimana 1*2*...*n
- hasil dari total akan disimpan pada f_num dari struct untuk digunakan pada saat pthread_create
```
pthread_t threads[20];
Cell_matrix c_m[20];
```
- threads[20] berfungsi untuk menyimpan ID thread yang digunakan dalam perhitungan faktorial dari sel matriks berjumlah 20 dari matriks ordo 4 x 5
- c_m[20] berfungsi untuk menyimpan hasil faktorial dari sel matriks berjumlah 20 dari matriks ordo 4 x 5
---
```
  for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            int result;
            c_m[i*5+j].num = ptr[i][j];
            pthread_create(&threads[i*5+j], NULL, factorial, &c_m[i*5+j]);
        }
    }
```
  - c_m[i*5+j].num berfungsi untuk menyimpan nilai setiap sel pada matriks shared memory secara berurutan dari baris 1 kolom 1 hingga baris 4 kolom 5. Variabel i sebagai baris dan j sebagai kolom. Dimana saat baris 1 yaitu i = 0 pengisian akan dimulai dari kolom 1 yaitu j = 0. Array dimulai dari 0 sehingga pengisian kolom dari 0,1,2,3,4 dan jika sudah kolom maksimal maka akan berpindah baris dari 0,1,2,3 
  - pthread_create berfungsi untuk menginisialisasi thread baru dan memulai eksekusi thread. c_m[i*5+j] berfungsi sebagai arg yaitu argumen khusus kepada fungsi void *factorial(*arg)
  ```
    printf("Matriks faktorial program cinta.c\n");
    for (int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            pthread_join(threads[i*5+j], NULL);
            printf("%llu ", c_m[i*5+j].f_num);
        }
        printf("\n");
    }
  ```
  - Berfungsi untuk menampilkan matriks dengan setiap selnya merupakan hasil faktorial dari angka sebelumnya
  - pthread_join berfungsi agar suatu thread dapat menunggu thread yang ditentukan dapat selesai sebelum melanjutkan eksekusi
  ---
  ```
    clock_t start = clock();
    clock_t end = clock();
  ```
  - start berfungsi mendapatkan waktu dimulainya eksekusi thread sedangkan end berfungsi mendapatkan waktu berakhirnya eksekusi thread
  ---

- ### Perintah 2-d
Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

```
    typedef struct{
        int num;
        unsigned long long f_num;
    } Cell_matrix;
  ```
  - struct Cell_matrix berfungsi untuk menyimpan angka yang akan difaktorialkan (num) dan angka hasil faktorial (f_num)
  - struct dibutuhkan karena fungsi faktorial digunakan pada thread
  ---
```
    void factorial(Cell_matrix *c_m, int m[], int n){
        unsigned long long total = 1;
        for (int i = 1; i<=m[n] ; i++) {
            total *= i;
        }
        c_m[n].f_num = total;
    }   
```
- Fungsi factorial untuk mendapatkan hasil faktorial dari angka setiap sel pada matriks di segmen shared memory dengan key = 1234
- c_m sebagai struct untuk menyimpan hasil faktorial dari angka pada sel matriks
- m sebagai array yang berisi angka yang akan difaktorialkan
- n sebagai indeks dari array m
- total diinisialisasi dengan 1 untuk perkalian pada looping for. Dimana 1*2*...*n
- hasil dari total akan disimpan pada f_num dari struct 
```
    int matrix[20];
    Cell_matrix cell_m[20];
```
- matrix[20] berfungsi untuk menyimpan angka yang digunakan dalam perhitungan faktorial dari sel matriks berjumlah 20 dari matriks ordo 4 x 5
- cell_m[20] berfungsi untuk menyimpan hasil faktorial dari sel matriks berjumlah 20 dari matriks ordo 4 x 5
---
```
    key_t key = 1234;
    key_t key2 = 5678;
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);    
    int (*ptr)[5] = (int (*)[5]) shmat(shmid, NULL, 0);
    int shmid2 = shmget(key2, sizeof(double), IPC_CREAT | 0666);    
    double *multithread_time;
	multithread_time = shmat(shmid2, NULL, 0);
```
  - key berfungsi sebagai kunci numerik dalam sistem operasi untuk mengidentifikasi segmen shared memory
  - shmid berfungsi sebagai identifier untuk mengakses segmen shared memory yang telah dibuat
  - shmget berfungsi untuk memmbuat atau mendapatkan segmen shared memory
  - shmat berfungsi untuk menghubungkan segmen shared memory ke alamat virtual suatu proses
  - ptr sebagai array untuk menyimpan array matrix pada segmen shared memory dengan key = 1234
  - multithread_time untuk menyimpan waktu yang dibutuhkan selama menjalankan thread. multithread_time berada pada segmen shared memory dengan key = 5678
  ```
  shmdt(ptr);
  shmdt(multithread_time);
  ```
  - shmdt berfungsi untuk melepaskan koneksi segmen shared memory
  ---
  ```
    printf("Matrix\n");
    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            matrix[i*5+j] = ptr[i][j];
            printf("%d ", matrix[i*5+j]);
        }
        printf("\n");
    }
  ```
  - Berfungsi untuk menampilkan matrix hasil perkalian pada segmen shared memory
  - matrix[i*5+j] berfungsi untuk menyimpan nilai setiap sel pada matriks shared memory secara berurutan dari baris 1 kolom 1 hingga baris 4 kolom 5. Variabel i sebagai baris dan j sebagai kolom. Dimana saat baris 1 yaitu i = 0 pengisian akan dimulai dari kolom 1 yaitu j = 0. Array dimulai dari 0 sehingga pengisian kolom dari 0,1,2,3,4 dan jika sudah kolom maksimal maka akan berpindah baris dari 0,1,2,3 
  ---
  ```
    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            factorial(cell_m, matrix, i*5+j);
        }
    }
  ```
  - Berfungsi untuk mendapatkan hasil faktorial melalui fungsi void factorial(Cell_matrix *c_m, int m[], int n) yang disimpan pada array matrix 
  ```
    for (int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            printf("%llu ", cell_m[i*5+j].f_num);
        }
        printf("\n");
    }
  ```
  - Berfungsi untuk menampilkan matriks dengan setiap selnya merupakan hasil faktorial dari angka sebelumnya
  ---
  ```
    clock_t start = clock();
    clock_t end = clock();
    double withoutThread_time = ((double)(end - start)) / CLOCKS_PER_SEC;

  ```
  - start berfungsi mendapatkan waktu dimulainya eksekusi normal tanpa thread sedangkan end berfungsi mendapatkan waktu berakhirnya eksekusi normal tanpa thread
  - withoutThread_time untuk menyimpan waktu yang dibutuhkan untuk mengeksekusi tanpa menggunakan thread
  ---

## Soal 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.

### Perintah 3-a
Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib).

Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

Pada program user.c berfungsi sebagai pengirim perintah seperti DECRYPT dan LIST melalui message queue

```
struct message_buffer {
    long type;
    char message[MAX_TEXT_SIZE];
    pid_t user_pid;
};

```
- struct berfungsi menyimpan rincian perintah dikirim melalui message queue
- message_type berfungsi menyimpan jenis perintah 
- message_text berfungsi menyimpan isi perintah yang akan diterima program receiver yaitu stream.c
- user_pid berfungsi untuk mengidentifikasi Process ID pengirim
```
key_t key;
int message_id;
struct message_buffer message;

if ((key = ftok("stream.c", 'B')) == -1) {
    perror("ftok");
    exit(1);
}

if ((message_id = msgget(key, 0644)) == -1) {
    perror("msgget");
    exit(1);
}

char input[MAX_TEXT_SIZE];
while (1) {
    printf("Type here : ");
    fgets(input, MAX_TEXT_SIZE, stdin);
    input[strcspn(input, "\n")] = '\0'; 
    if (strncmp(input, "ADD ", 3) == 0 || strncmp(input, "DECRYPT", 7) == 0 || strncmp(input, "LIST", 4) == 0 || strncmp(input, "PLAY ", 4) == 0 ) {
        pid_t user_pid = getpid();
        message.user_pid = user_pid;
        strncpy(message.message, input, MAX_TEXT_SIZE);

    } else {
        message.user_pid = 0; 
        strcpy(message.message, input);
    }

    message.type = 1;

    if (msgsnd(message_id, &message, sizeof(message) - sizeof(long), 0) == -1) {
        perror("msgsnd");
        exit(1);
    }
}
```
- key berfungsi menyimpan kunci numerik dalam sistem operasi untuk mengakses message queue
- msgget berfungsi untuk mengakses message queue pada sistem. Hasil dari message berupa ID message queue disimpan pada message_id
- message berfungsi sebagai struct untuk menyimpan perintah yang dikirim melalui message queue
- input untuk menyimpan isi perintah
- fgets berfungsi mendapatkan input perintah 
- `input[strcspn(input, "\n")] = '\0'` untuk menghapus karakter newline pada input perintah
- msgsnd untuk mengirim perintah melalui message queue dari struct message dengan ID dari msgget 
---
Program stream.c sebagai receiver yang menerima perintah dari program user.c dan menjalankan fungsi sesuai perintah yang diterima melalui message queue
```
    key_t key;
    int message_id;
    struct message_buffer message;

    if ((key = ftok("stream.c", 'B')) == -1) {
        perror("ftok");
        exit(1);
    }

    if ((message_id = msgget(key, 0644 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    while (1) {
        if (msgrcv(message_id, &message, sizeof(message) - sizeof(long), 0, 0) == -1) {
            perror("msgrcv");
            exit(1);
        }
    }

```
- key berfungsi menyimpan kunci numerik dalam sistem operasi untuk mengakses message queue
- msgget berfungsi untuk mengakses message queue pada sistem. Hasil dari message berupa ID message queue disimpan pada message_id
- message berfungsi sebagai struct untuk menyimpan perintah yang dikirim melalui message queue
- msgrcv untuk menerima perintah melalui message queue dan disimpan pada struct message dengan ID dari msgget 
---

### Perintah 3-b
User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.

---
Metode rot13
```
char* rot13(char *str) {
    char c;
    for(int i=0; str[i] != '\0'; i++) {
        c = str[i];
        if(c >= 'A' && c <= 'Z') {
            c = ((c - 'A') + 13) % 26 + 'A';
        } else if(c >= 'a' && c <= 'z') {
            c = ((c - 'a') + 13) % 26 + 'a';
        }
        str[i] = c;
    }
    return str;
}

```
- char str berfungsi untuk menyimpan nama lagu yang di decrypt
- c untuk menyimpan karakter pada string yang akan di looping
- `if(c >= 'A' && c <= 'Z')` memeriksa apakah c merupakan huruf kapital
- `else if(c >= 'a' && c <= 'z')` memeriksa apakah c merupakan huruf kecil
- `c = ((c - 'A') + 13) % 26 + 'A';` dan `c = ((c - 'a') + 13) % 26 + 'a'; ` menggeser sejauh 13 karakter selanjutnya dan kembali ke karakter awal A atau a jika melebihi maksimal yaitu Z atau z
---
Metode base64
```
    char* base64(const char* input) {
        const char* BASE64_ALPHABET ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
 
        size_t len = strlen(input);
        size_t padding = 0;
 
        if (input[len-1] == '=') {
            padding++;
            if (input[len-2] == '=') {
                padding++;
            }
        }
 
        size_t outlen = len * 3 / 4 - padding;
 
        char* output = malloc(outlen + 1);
 
        unsigned char buffer[4];
        size_t j = 0;
 
        for (size_t i = 0; i < len; i += 4) {
            buffer[0] = strchr(BASE64_ALPHABET, input[i]) - BASE64_ALPHABET;
            buffer[1] = strchr(BASE64_ALPHABET, input[i+1]) - BASE64_ALPHABET;
            buffer[2] = strchr(BASE64_ALPHABET, input[i+2]) - BASE64_ALPHABET;
            buffer[3] = strchr(BASE64_ALPHABET, input[i+3]) - BASE64_ALPHABET;
 
            output[j++] = (char) ((buffer[0] << 2) | (buffer[1] >> 4));
            if (j < outlen) {
               output[j++] = (char) ((buffer[1] << 4) | (buffer[2] >> 2));
            }
            if (j < outlen) {
                output[j++] = (char) ((buffer[2] << 6) | buffer[3]);
            }
        }
 
        output[outlen] = '\0';
 
        return output;
    }
```
- char input berfungsi untuk menyimpan nama lagu yang di decrypt
- `const char* BASE64_ALPHABET ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";` merupakan string yang digunakan untuk decrypt 
- len untuk menyimpan panjang string input dari fungsi strlen
- padding untuk menyimpan jumlah karakter padding yang ditambahkan ke output
- outlen untuk menyimpan panjang output setelah decrypt
- mengalokasikan memori dengan malloc untuk output
- buffer brefungsi untuk menyimpan karakter input yang diproses 
- `output[j++] = (char) ((buffer[0] << 2) | (buffer[1] >> 4));` mengubah karakter pertama sesuai metode base64 menggunakan operasi bitwise
- `output[j++] = (char) ((buffer[1] << 4) | (buffer[2] >> 2));` mengubah karakter pertama sesuai metode base64 menggunakan operasi bitwise
- `output[j++] = (char) ((buffer[2] << 6) | buffer[3]);` mengubah karakter pertama sesuai metode base64 menggunakan operasi bitwise
- `output[outlen] = '\0';` karakter NULL sebagai penanda akhir string
---
Metode hex
```
    char* hex(char *str) {
        char hexChar[3];
        int i, len = strlen(str);
        char *newStr = malloc(len/2 + 1);

        for(i=0; i<len-1; i+=2) {
            sprintf(hexChar, "%c%c", str[i], str[i+1]);
            int value = (int)strtol(hexChar, NULL, 16);
            newStr[i/2] = value;
        }
        newStr[i/2] = '\0';
 
        return newStr;
    }   
```
- char str berfungsi untuk menyimpan nama lagu yang di decrypt
- len untuk menyimpan panjang string str dari fungsi strlen
- mengalokasikan memori untuk menyimpan output dengan pointer newStr
- Iterasi looping setiap 2 karakter dengan i sebagai indeks karakter pada string
- value untuk menyimpan nilai dari 2 karakter diubah pada hexChar dengan fungsi strtol berdasarkan basis heksadesimal
---
```
int compare_strings(const void* a, const void* b) {
    const char** string_a = (const char**) a;
    const char** string_b = (const char**) b;
 
    return strcasecmp(*string_a, *string_b);
}
```
- Berfungsi untuk membandingkan antara string_a dengan string_b merupakan string yang sama persis secara case insensitive dengan menggunakan strcasecmp
---
```
    void com_decrypt(const char *input_file, const char *output_file) {
        FILE *fp;
        FILE *out_fp;
        char buffer[100000];

        struct json_object *parsed_json;
        struct json_object *method;
        struct json_object *song;

        size_t n_objects;
        size_t i;

        fp = fopen(input_file, "r");
        fread(buffer, 100000, 1, fp);
        fclose(fp);

        parsed_json = json_tokener_parse(buffer);

        if (parsed_json == NULL) {
            fprintf(stderr, "Error: failed to parse JSON\n");
            return;
        }

        out_fp = fopen(output_file, "w");

        if (json_object_is_type(parsed_json, json_type_array)) {
            n_objects = json_object_array_length(parsed_json);

            char **decrypted_songs = (char **) malloc(n_objects * sizeof(char *));

            for (i = 0; i < n_objects; i++) {
                struct json_object *obj = json_object_array_get_idx(parsed_json, i);

                json_object_object_get_ex(obj, "method", &method);
                json_object_object_get_ex(obj, "song", &song);

                char *decrypted_song;
                if (strcmp(json_object_get_string(method), "method_rot13") == 0) {
                    decrypted_song = method_rot13(strdup(json_object_get_string(song)));
                } else if (strcmp(json_object_get_string(method), "base64") == 0) {
                    decrypted_song = base64(strdup(json_object_get_string(song)));
                } else if (strcmp(json_object_get_string(method), "hex") == 0) {
                    decrypted_song = hex(strdup(json_object_get_string(song)));
                } else {
                    decrypted_song = strdup(json_object_get_string(song));
                }

                decrypted_songs[i] = decrypted_song;
            }

            qsort(decrypted_songs, n_objects, sizeof(char *), compare_strings);

            for (i = 0; i < n_objects; i++) {
                fprintf(out_fp, "%s\n", decrypted_songs[i]);
                free(decrypted_songs[i]);
            }

            free(decrypted_songs);
        }

        fclose(out_fp);
    }
```
- fp sebagai pointer untuk membaca dari input_file
- fp_out sebagai pointer untuk menulis ke output_file
- buffer untuk menyimpan string yang dibaca dengan fread pada pointer fp
---

### Perintah 3-c
Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt

---
```
void print_playlist() {
    FILE* playlist_fp = fopen("playlist.txt", "r");
    if (playlist_fp == NULL) {
        perror("Error: failed to open playlist file");
        return;
    }

    char line[MAX_TEXT_SIZE];
    while (fgets(line, sizeof(line), playlist_fp)) {
        printf("%s", line);
    }

    fclose(playlist_fp);
}
```
- Berfungsi menampilkan daftar dari file playlist.txt
- playlist_fp sebagai pointer untuk membaca file dari file playlist.txt
- fgets untuk membaca string dengan pointer playlist_fp
- fclose untuk menutup playlist_fp
---

### Perintah 3-d
User juga dapat mengirimkan perintah "PLAY <SONG>"

```
void play(char *song_name, pid_t user_pid) {
    char *start_quote = strchr(song_name, '"');
    if (start_quote == NULL) {
        printf("Error: invalid song name format\n");
        return;
    }
    char *end_quote = strchr(start_quote + 1, '"');
    if (end_quote == NULL) {
        printf("Error: invalid song name format\n");
        return;
    }
    strncpy(song_name, start_quote + 1, end_quote - start_quote - 1);
    song_name[end_quote - start_quote - 1] = '\0';

    FILE *playlist_fp = fopen("playlist.txt", "r");
    if (playlist_fp == NULL) {
        perror("Error: failed to open playlist file");
        return;
    }

    char line[MAX_TEXT_SIZE];
    int n_songs = 0;

    while (fgets(line, sizeof(line), playlist_fp)) {
        if (strcasestr(line, song_name)) {
            n_songs++;
        }
    }

    fclose(playlist_fp);

    if (n_songs == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", song_name);
    } else if (n_songs == 1) {
        playlist_fp = fopen("playlist.txt", "r");
        if (playlist_fp == NULL) {
            perror("Error: failed to open playlist file");
            return;
        }
        while (fgets(line, sizeof(line), playlist_fp)) {
            if (strcasestr(line, song_name)) {
            printf("USER %d PLAYING %s", (int)user_pid, line);
                break;
            }
        }
        fclose(playlist_fp);
    } else {
        playlist_fp = fopen("playlist.txt", "r");
        if (playlist_fp == NULL) {
            perror("Error: failed to open playlist file");
            return;
        }
        int song_number = 1;
        while (fgets(line, sizeof(line), playlist_fp)) {
            if (strcasestr(line, song_name)) {
                printf("%d. %s", song_number, line);
                song_number++;
            }
        }
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\"\n", n_songs, song_name);
        fclose(playlist_fp);
    }
```
Fungsi ini memiliki tujuan untuk mencari dan menampilkan lagu dari (playlist) yang terdapat pada sebuah file teks bernama "playlist.txt".

`if (start_quote == NULL) {...}` untuk memeriksa apakah variabel "start_quote" tidak menunjuk ke suatu lokasi dalam memori. 

Jika iya, maka menampilkan pesan error "Error: invalid song name format" dan fungsi ini diakhiri dengan perintah "return".

`if (end_quote == NULL) {...}` untuk memeriksa apakah variabel "end_quote" tidak menunjuk ke suatu lokasi dalam memori. Jika iya, maka menampilkan pesan error "Error: invalid song name format" dan fungsi ini diakhiri dengan perintah "return".

`FILE *playlist_fp = fopen("playlist.txt", "r");` untuk mendeklarasikan variabel pointer "playlist_fp" yang bertipe FILE dan membuka file "playlist.txt" dengan mode "r" (read) menggunakan fungsi "fopen". Jika gagal membuka file, maka menampilkan pesan error menggunakan fungsi "perror" dan fungsi ini diakhiri dengan perintah "return".

Fungsi ini mencari tanda kutip pertama dan terakhir dalam string "song_name" untuk menentukan nama lagu yang akan diputar. 

Jika format "song_name" tidak valid, maka akan ditampilkan pesan error dan fungsi akan berhenti.

Jika format "song_name" valid, maka string "song_name" akan diperbarui untuk hanya berisi nama lagu tanpa tanda kutip.

Lalu, fungsi membuka file "playlist.txt" dan membaca setiap baris dalam file untuk mencari lagu yang mengandung nama yang sama dengan "song_name". 

Setiap baris yang sesuai akan dihitung dan jumlahnya disimpan dalam variabel "n_songs".

### Perintah 3-e
User juga dapat menambahkan lagu ke dalam playlist dengan syarat sebagai berikut:

- User mengirimkan perintah
- User dapat mengedit playlist secara bersamaan tetapi lagu yang ditambahkan tidak boleh sama. Apabila terdapat lagu yang sama maka sistem akan meng-output-kan “SONG ALREADY ON PLAYLIST”

```
void update_playlist() {
    const char* filename = "playlist.txt";
    const int MAX_LINES = 1500;
    char* lines[MAX_LINES];
    int num_lines = 0;

    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Error: Could not open file %s\n", filename);
        return;
    }
    char buffer[100000];
    while (fgets(buffer, sizeof(buffer), file) != NULL) {
        buffer[strcspn(buffer, "\r\n")] = '\0'; 
        lines[num_lines] = strdup(buffer); 
        num_lines++;
    }
    fclose(file);

    qsort(lines, num_lines, sizeof(char*), compare_strings);

    file = fopen(filename, "w");
    if (file == NULL) {
        printf("Error: Could not open file %s\n", filename);
        return;
    }
    for (int i = 0; i < num_lines; i++) {
        fprintf(file, "%s\n", lines[i]);
        free(lines[i]); 
    }
    fclose(file);
}
```
Pada bagian ini, pertama file playlist akan dibuka, lalu akan dicek apakah lagu sudah ada di dalam playlist atau belum. Selanjutnya, jika belum ditemukan, pointer akan langsung berpindah ke akhir untuk menambahkan lagu. Terakhir, playlist akan diupdate dengan fungsi update_playlist()

```
void update_playlist() {
    const char* filename = "playlist.txt";
    const int MAX_LINES = 1500;
    char* lines[MAX_LINES];
    int num_lines = 0;

    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Error: Could not open file %s\n", filename);
        return;
    }
    char buffer[100000];
    while (fgets(buffer, sizeof(buffer), file) != NULL) {
        buffer[strcspn(buffer, "\r\n")] = '\0'; 
        lines[num_lines] = strdup(buffer); 
        num_lines++;
    }
    fclose(file);

    qsort(lines, num_lines, sizeof(char*), compare_strings);

    file = fopen(filename, "w");
    if (file == NULL) {
        printf("Error: Could not open file %s\n", filename);
        return;
    }
    for (int i = 0; i < num_lines; i++) {
        fprintf(file, "%s\n", lines[i]);
        free(lines[i]); 
    }
    fclose(file);
}
```

### Perintah 3-f
Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.
Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.

```
sem_t *sem;
if ((sem = sem_open("/thesemaphore", O_CREAT, 0644, MAX_ACTIVE_USERS)) == SEM_FAILED) {
    perror("sem_open");
    exit(1);
}
```
Untuk membatasi user, kita menggunakan semaphore, dimana MAX_ACTIVE_USERS sudah dibatasi maksimal 2 users.

```
int found_user = 0;
for (int i = 0; i < active_users_count; i++) {
    if (active_users[i] == message.pid) {
        found_user = 1;
        break;
    }
}

if (!found_user && active_users_count == MAX_ACTIVE_USERS) {
    printf("STREAM SYSTEM OVERLOAD: User %d cannot send commands at the moment.\n", message.pid);
    continue;
}

if (!found_user) {
    if (sem_wait(sem) == -1) {
        perror("sem_wait");
        exit(1);
    }
    active_users[active_users_count++] = message.pid;
}
```
Pada bagian ini digunakan untuk mencari apakah suatu pengguna sudah ada dalam array active_users atau belum.

### Perintah 3-g
Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

```
else {
    printf("UNKNOWN COMMAND: %s\n", message.command);
}
```
Pada bagian ini, pesan yang diterima akan dicek dengan seluruh command yang ada, jika tidak ada yang sesuai, maka akan ditampilkan pesan "UNKNOWN COMMAND"

  ---
## Soal 4
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 

- ### Perintah 4-a
Download dan unzip file tersebut dalam kode c bernama unzip.c.

Pada proses download dan unzip berikut menggunakan sistem fork(), yang menggunakan materi pada modul 2 praktikum.

```
int main() {
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); 
  }

  if (child_id == 0) {

    char *argv[] = {"wget", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", "-O", "hehe.zip", "-q", NULL};
    execv("/usr/bin/wget", argv);
  } else {
    while ((wait(&status)) > 0);
    char *arg[] = {"unzip", "-q", "hehe.zip", NULL};
    execv("/usr/bin/unzip", arg);
  }
}
```
Keterangan :
- wget : perintah untuk download
- unzip : untuk mengekstrak file
- -q : untuk menjalankan perintah dalam mode quiet

Deklarasi variabel ``child_id `` digunakan untuk menyimpan nilai dari proses fork()

Deklarasi variabel ``status `` digunakan untuk menyimpan status dari proses yang dijalankan

```
child_id < 0
```
Apabila child_id bernilai kurang dari nol maka, proses gagal. Program akan diakhiri dengan pemanggilan fungsi exit() dengan argumen `EXIT_FAILURE`.

```
child_id == 0
```
Apabila nilai child_id sama dengan nol, maka program sedang berjalan pada `CHILD PROCESS`. 

Pada child process digunakan perintah "wget" dengan argumen tautan serta nama file yang ingin di download. 

Perintah download dilakukan pada child process, karena child process akan di proses lebih dahulu daripada parent process.

```
child_id > 0
```
Apabila nilai child_id lebih besar dari nol, maka program sedang berjalan pada `PARENT PROCESS`. 

Proses ini menggunakan fungsi wait() yang dimana fungsi tersebut akan menghentikan parent process sampai child process selesai dieksekusi.

Dengan argumen status yang dipakai pada fungsi wait() akan menunjukkan status proses yang sedang berjalan, sehingga menghubungkan parent process dan child process. 

Pada parent process digunakan perintah "unzip" dengan argumen nama file untuk mengekstrak file tersebut. 

- ### Perintah 4-b
Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. 

Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. 

Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase.

Akan tetapi, file bisa saja tidak semua lowercase. 
 
File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.

Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format : 

`extension (2)`

`extension (3)`

...

Fungsi get_ext untuk mendapatkan ekstensi dari suatu file :
```
const char *get_ext(char *dir){
    const char *name = strrchr(dir, '.');

    //mengembalikan string kosong
    if(!name || name == dir)
        return "";

    return name + 1;
}
```
Fungsi strrchr() digunakan untuk mencari karakter "." pada suatu string. Jika karakter tersebut ditemukan, maka pointer yang menunjuk pada karakter tersebut akan disimpan dalam variabel "name".

`return name + 1` adalah pernyataan return yang mengembalikan string ekstensi file. Pointer name yang menunjuk pada karakter titik digeser satu karakter ke depan dengan operator "+ 1", sehingga pointer tersebut menunjuk pada karakter setelah karakter yang dicari (.), yang merupakan ekstensi dari suatu file.


Fungsi comp_ext untuk membandingkan ekstensi :
```
int comp_ext(char ext[]) {
    for(int i=0; i<ext_count;i++){
        if(strcmp(ext, extensions[i]) == 0)
            return i;
    }
    return -1;
}
```
Fungsi strcmp() digunakan untuk membandingkan nilai "ext" dengan "extensions" pada index ke-"i"

Ekstensi yang didapatkan pada fungsi "get_ext" akan di bandingkan dengan ekstensi yang berada pada file "ekstensions.txt" yang sudah didownload sebelumnya. 

Dilakukan looping sebanyak ekstensi yang didapatkan dan akan dibandingkan pada file "extensions".

Apabila ekstensi tersebut sesuai dengan ekstensi yang diminta pada "extensions" maka akan dikembalikan nilai "i". Namun, apabila ekstensi tersebut tidak sesuai dengan ekstensi pada file "extensions" maka akan dikembalikan nilai "-1".

Fungsi get_log_time untuk mendapatkan waktu lokal saat ini :
```
char get_log_time(char *time_str) {
    time_t rawtime;
    struct tm * timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(time_str, 100, "%d-%m-%Y %H:%M:%S", timeinfo);
}
```
``time(&rawtime);`` digunakan untuk mendapatkan waktu saat ini dalam bentuk UNIX timestamp dan disimpan pada variabel "rawtime".

``localtime(&rawtime)`` digunakan untuk mengonversi UNIX timestamp ke dalam struktur waktu lokal yang disimpan pada variabel "timeinfo".

Fungsi strftime() untuk mengonversi waktu lokal ke dalam bentuk string. Waktu lokal yang disimpan ke variabel timeinfo akan dikonversi ke string dengan format `%d-%m-%Y %H:%M:%S`.


Fungsi process_extension untuk memproses file ekstensi:
```
void* process_extension() { 
    FILE *filePointer = fopen(ext_file, "r");

    if (filePointer == NULL) {
        printf("Gagal membuka file!");
        exit(EXIT_FAILURE);
    }

    char buffer[MAX_PATH_LENGTH];
    char **lines = NULL;
    int index=0;

    while (fgets(buffer, MAX_PATH_LENGTH, filePointer)) {
        lines = realloc(lines, sizeof(char*) * (ext_count + 1));
        lines[ext_count] = malloc(sizeof(char) * (strlen(buffer) + 1));

        strncpy(lines[index], buffer, strlen(buffer) - 2);
        ext_dir[index] = 0;

        strcpy(extensions[index], lines[index]);
        index++;
        ext_count++;
    }
    fclose(filePointer);

    chdir("./");

    strcpy(logname, "./");
    strcat(logname, "/log");

    strcpy(log_name, logname);
    strcat(log_name, ".txt");

    sprintf(filename, "categorized");
    mkdir(filename, 0777);

    filePointer = fopen(log_name, "a+");

    get_log_time(log_time);
    fprintf(filePointer, "%s MADE %s\n", log_time, filename);

    fclose(filePointer);

    for (int i = 0; i < ext_count; i++) {

        sprintf(filename, "categorized/%s", extensions[i]);
        mkdir(filename, 0777);

        filePointer = fopen(log_name, "a+");
        get_log_time(log_time);
        fprintf(filePointer, "%s MADE %s\n", log_time, filename);

        fclose(filePointer);

    }
    sprintf(filename, "categorized/other");
    mkdir(filename, 0777);

    filePointer = fopen(log_name, "a+");
    get_log_time(log_time);
    fprintf(filePointer, "%s MADE %s\n", log_time, filename);

    fclose(filePointer);
}
```
Fungsi diatas digunakan untuk memproses file ekstensi yang berisi daftar ekstensi file yang akan dikategorikan. 

``FILE *filePointer = fopen(ext_file, "r");`` digunakan untuk membuka file yang berisi ekstensi-ekstensi yang ditentukan(di download).

```
if (filePointer == NULL) {
    printf("Gagal membuka file!");
    exit(EXIT_FAILURE);
}
```
Apabila file gagal dibuka maka fungsi akan memberikan pesan gagal membuka file dan akan keluar dari program.

Fungsi fgets() digunakan untuk membaca setiap baris dari file ekstensi dan menyimpannya ke dalam buffer "buffer" dengan maksimal panjang sebesar "MAX_PATH_LENGTH".

Setiap baris yang dibaca kemudian disimpan ke dalam variabel lines yang sebelumnya telah dialokasikan memori dinamis. 

Setiap baris pada lines diinisialisasi dengan memanggil fungsi malloc() untuk mengalokasikan memori sebesar "strlen(buffer) + 1".

Kemudian, setiap baris yang berhasil dibaca dan diproses akan disimpan di dalam array extensions pada indeks yang sama dengan lines[index]. 

Selain itu, variabel index dan ext_count akan diincrement untuk menyimpan informasi bahwa sudah terbaca ext_count baris pada file ekstensi.

``fclose(filePointer);`` menutup file pointer yang sudah dibuka sebelumnya.

`chdir("./");` mengubah direktori ke direktori yang sama dengan program.

```
strcpy(logname, "./");
strcat(logname, "/log");
```
Membuat string "logname" dan mengisi dengan "./log".

```
strcpy(log_name, logname);
strcat(log_name, ".txt");
```
Mengisi string "log_name" dengan string "logname" dan menambahkan ekstensi .txt sehingga menjadi ./log.txt.

```
sprintf(filename, "categorized");
mkdir(filename, 0777);
```
Membuat direktori baru dengan nama "categorized".

```
filePointer = fopen(log_name, "a+");
```
Membuka file pointer dengan mode "a+" (append dan read/write) untuk menulis log ke file log_name.

```
get_log_time(log_time);
fprintf(filePointer, "%s MADE %s\n", log_time, filename);
```
Mendapatkan waktu saat ini dengan fungsi get_log_time() dan menuliskan log ke file "log_name".

`fclose(filePointer);` menutup file pointer.

```
for (int i = 0; i < ext_count; i++) {

    sprintf(filename, "categorized/%s", extensions[i]);
    mkdir(filename, 0777);

    filePointer = fopen(log_name, "a+");
    get_log_time(log_time);
    fprintf(filePointer, "%s MADE %s\n", log_time, filename);

    fclose(filePointer);

}
```
Untuk setiap ekstensi yang telah dibaca dari file ekstensi sebelumnya, membuat direktori baru dengan nama sesuai dengan ekstensi file tersebut, dan menulis log ke file log_name.

```
sprintf(filename, "categorized/other");
mkdir(filename, 0777);

filePointer = fopen(log_name, "a+");
get_log_time(log_time);
fprintf(filePointer, "%s MADE %s\n", log_time, filename);

fclose(filePointer);
```
Membuat direktori baru dengan nama di dalam direktori categorized dengan nama "other". Dicatat ke file "log_time".

Jadi, fungsi process_extension bertujuan untuk membaca file ekstensi, lalu membuat direktori baru untuk setiap ekstensi yang ditemukan dalam file ekstensi, serta membuat direktori baru categorized/other untuk file yang tidak memiliki ekstensi tertentu. 

Fungsi ini juga menuliskan log ke file log.txt untuk setiap proses yang dilakukan.

Fungsi scan_files dilakukan untuk scanning direktori dan subdirektori :
```
void *scan_files(void *arg) {
    DIR *dir;
    struct dirent *ent;
    char path[MAX_PATH_LENGTH];
    char cmd[9000];

    if ((dir = opendir((char*) arg)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_type == DT_DIR) {
                if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {  // directory  
                    continue;        
                } 
                sprintf(path, "%s/%s", (char*) arg, ent->d_name);

                get_log_time(log_time);
                sprintf(cmd, "echo '%s ACCESSED %s' >> %s", log_time, path, log_name);
                system(cmd);

                pthread_t tid;
                pthread_create(&tid, NULL, scan_files, (void*) path);
                pthread_join(tid, NULL);

            }
            else {
                sprintf(cmd, "echo '%s/%s' >> temp_file.txt", (char*) arg,ent->d_name);
                system(cmd);    
            }
        }
        closedir(dir);
    }
}
```
Fungsi opendir() digunakan untuk membuka direktori yang diberikan.

Lalu dilakukan looping dengan memeriksa setiap isi dari direktori di dalamnya dengan fungsi readdir(). 

Setiap file yang ditemukan akan dicatat di dalam file "temp_file.txt" menggunakan perintah sistem echo dengan format `<path>/<nama_file>`.

Jika sebuah subdirektori ditemukan, fungsi akan membuat thread baru dengan menggunakan fungsi pthread_create(). Thread baru akan menjalankan fungsi scan_files() lagi pada subdirektori tersebut. 

Selain itu, fungsi akan mencatat akses ke direktori tersebut pada file log dengan format "<timestamp> ACCESSED <path>" menggunakan perintah sistem echo.

`closedir(dir);` tutup direktori yang dibuka.

Fungsi read_max digunakan untuk membaca nilai maksimum yang telah disimpan dalam file :
```
void *read_max() {
    FILE *filePointer ;
    char line[255];
    int i = 0;

    filePointer = fopen("./max.txt", "r");

    if (filePointer == NULL) {
        printf("Gagal membuka file!");
        exit(EXIT_FAILURE);
    }

    while (fgets(line, sizeof(line), filePointer) ) {
        max_values = atoi(line);
    }
    fclose(filePointer);

    scan_files(FILES);
}
```
`FILE *filePointer ;` diinisialisasi untuk membuka file "max.txt". Dan apabila tidak bisa dibuka maka program akan menampilkan pesan kesalahan dan keluar dari program.

Selanjutnya, fungsi akan membaca nilai dalam file max.txt menggunakan fungsi fgets dalam loop while. Fungsi atoi akan digunakan untuk mengubah nilai dalam bentuk string menjadi integer, yang kemudian akan disimpan dalam variabel "max_values".

Setelah nilai max_values berhasil dibaca, fungsi akan memanggil fungsi scan_files dengan argumen FILES. Scan_files bertanggung jawab untuk melakukan scanning pada file dan folder dari path yang diberikan, dan mengkategorikan file-file tersebut sesuai dengan ekstensi masing-masing.


Fungsi move_files digunakan untuk memindahkan file-file yang sudah dikelompokkan :
```
void *move_files() {
    FILE *filePointer;
    char cmd[9000];
    char ext[MAX_PATH_LENGTH];
    char line[MAX_PATH_LENGTH];
    char buffer[5000];

    filePointer = fopen("./temp_file.txt", "r");

    while(fgets(line, MAX_PATH_LENGTH, filePointer)) {
        line[strlen(line)-1] = '\0';

        sprintf(ext, "%s", get_ext(line));

        for(int i = 0; ext[i]; i++){
            ext[i] = tolower(ext[i]);
        }

        if (comp_ext(ext) == -1) {
            sprintf(cmd, "cp '%s' ./categorized/other", line);
            system(cmd);

            get_log_time(log_time);
            sprintf(cmd, "echo '%s MOVED other file : %s > ./categorized/other' >> %s", log_time, line, log_name);
            system(cmd);
        }
        else { 
            if (ext_dir[comp_ext(ext)] < max_values) {
                sprintf(cmd, "cp '%s' ./categorized/%s", line, ext);
                system(cmd);

                ext_dir[comp_ext(ext)]++;

                get_log_time(log_time);
                sprintf(cmd, "echo '%s MOVED %s file : %s > ./categorized/%s' >> %s", log_time, ext, line, ext, log_name);
                system(cmd);
            }
            else {
                int num_ext = ext_dir[comp_ext(ext)] / max_values + 1;
                sprintf(buffer, "./categorized/%s (%d)", ext, num_ext);

                if(access(buffer, F_OK) != 0){
                    get_log_time(log_time);
                    sprintf(cmd, "echo '%s MADE %s' >> %s", log_time, buffer, "./log.txt");
                    system(cmd);
                }

                mkdir(buffer, 0777);

                sprintf(cmd, "cp '%s' './categorized/%s (%d)'", line, ext, num_ext);
                system(cmd);

                ext_dir[comp_ext(ext)]++;

                get_log_time(log_time);
                sprintf(cmd, "echo '%s MOVED %s file : %s > .%s' >> %s", log_time, ext, line, buffer, log_name);
                system(cmd);
            }
        }
    }
}
```
`filePointer = fopen("./temp_file.txt", "r");` digunakan untuk membaca file "temp_file.txt" yang dibuat pada fungsi scan_files().

Fungsi diatas akan membaca file "temp_file.txt" baris per baris. Setiap baris yang dibaca akan diambil ekstensinya dengan fungsi get_ext() dan diubah menjadi lowercase dengan menggunakan loop for `ext[i] = tolower(ext[i]);`.

Kemudian, fungsi comp_ext() akan dipanggil untuk mengecek apakah ekstensi tersebut termasuk ekstensi yang harus dikategorikan atau tidak.

```
if (comp_ext(ext) == -1) {
    sprintf(cmd, "cp '%s' ./categorized/other", line);
    system(cmd);

    get_log_time(log_time);
    sprintf(cmd, "echo '%s MOVED other file : %s > ./categorized/other' >> %s", log_time, line, log_name);
    system(cmd);
}
```
Kalau ekstensi tersebut tidak termasuk ekstensi yang harus dikategorikan, maka file akan dipindahkan ke dalam direktori "./categorized/other".

Jika ekstensi tersebut termasuk ekstensi yang harus dikategorikan, maka akan dilakukan pengecekan apakah jumlah file dalam direktori "./categorized/" yang sesuai dengan ekstensi tersebut sudah mencapai batas maksimum yang ditentukan di file max.txt.

Jika belum mencapai batas maksimum, maka file akan dipindahkan ke dalam direktori "./categorized/[extension]". Dan akan dicatat pada log file.

`ext_dir[comp_ext(ext)]++;` digunakan untuk mengupdate dengan menambahkan 1 pada elemen array ext_dir yang sesuai dengan indeks ekstensi tersebut.

Namun jika jumlah file dalam direktori untuk ekstensi tersebut sudah mencapai batas maksimum, maka akan dibuat direktori baru dengan format "./categorized/[extension](n)" dimana n adalah angka yang menunjukkan urutan pembuatan direktori baru. 

File yang akan dipindahkan akan dipindahkan ke direktori baru tersebut dan dicatat pada file log. Jumlah file dalam direktori untuk ekstensi tersebut akan diupdate dengan menambahkan 1 pada elemen array ext_dir yang sesuai dengan indeks ekstensi tersebut.

Fungsi count_extension digunakan untuk menghitung jumlah file pada setiap ekstensi file :
```
void *count_extension() {
    char cmd[9000];
    strcpy(extensions[ext_count], "other");
    DIR *dir;
    struct dirent *ent;
    dir = opendir("./categorized/other");

    get_log_time(log_time);
    sprintf(cmd, "echo '%s ACCESSED ./categorized/other' >> %s", log_time, log_name);
    system(cmd);

    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_type == DT_REG) {
            ext_dir[ext_count]++;
        }
    }

    closedir(dir);
    ext_count++;

    for(int i=0;i<ext_count-1; i++){
        for(int j=0; j<ext_count-i-1;j++){
            if(ext_dir[j] > ext_dir[j+1]){
                int temp;
                temp = ext_dir[j];
                ext_dir[j] = ext_dir[j+1];
                ext_dir[j+1] = temp;

                strcpy(filename, extensions[j]);
                strcpy(extensions[j], extensions[j+1]);
                strcpy(extensions[j+1], filename);
            }
        }
    }
    
    for(int i=0; i<ext_count; i++){
        printf("%s : %d\n", extensions[i], ext_dir[i]);
    }
}
```
`dir = opendir("./categorized/other");` direktori "./categorized/other" dibuka menggunakan fungsi opendir(), dan setiap file dalam direktori tersebut dihitung menggunakan loop while dengan menggunakan increment. Setelah selesai, direktori ditutup.

Dilakukan sorting dengan bubble sort. Pada setiap iterasi loop, dua elemen yang bersebelahan dibandingkan, dan jika jumlah file pada elemen pertama lebih kecil dari jumlah file pada elemen kedua, maka kedua elemen tersebut ditukar. Untuk mengurutkan data dari yang kecil sampai ke besar.

Setelah diurutkan, program akan mencetak jumlah file dari setiap ekstensi.

- ### Perintah 4-c
Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.

``extension_a : banyak_file``

``extension_b : banyak_file``

``extension_c : banyak_file``

``other : banyak_file``

Output:
```
py : 0
xyz : 0
js : 3
png : 4
jpg : 9
other : 41
emc : 69
txt : 74
```


- ### Perintah 4-d
Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.

```
int main() {
    pthread_t threads;

    process_extension();

    pthread_create(&threads, NULL, read_max, NULL); 
    pthread_join(threads, NULL);

    pthread_create(&threads, NULL, move_files, NULL);
    pthread_join(threads,NULL);

    pthread_create(&threads, NULL, count_extension, NULL);
    pthread_join(threads,NULL);

    system("rm temp_file.txt");

    return 0;
}
```
Thread pertama akan menjalankan fungsi read_max untuk membaca nilai "max_values" dari file max.txt

Thread kedua akan menjalankan fungsi move_files untuk memindahkan file-file ke folder categorized berdasarkan ekstensi file. 

Thread ketiga akan dijalankan untuk menghitung jumlah file di setiap folder berdasarkan ekstensi file.

- ### Perintah 4-e
Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.

``DD-MM-YYYY HH:MM:SS ACCESSED [folder path]``

``DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]``

``DD-MM-YYYY HH:MM:SS MADE [folder name]``

Hasil :
```
13-05-2023 17:04:16 ACCESSED ./files/sdauh kasdks.ZIP
13-05-2023 17:04:16 MADE categorized/other
13-05-2023 17:04:16 MADE ./categorized/emc (6)
```

- ### Perintah 4-f
Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.

- Untuk menghitung banyaknya ACCESSED yang dilakukan.
- Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
- Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.

```
int accessed() {
    FILE *filePointer;
    char line[MAX_PATH_LENGTH];
    int access = 0;

    filePointer = fopen("log.txt", "r");
    if (filePointer == NULL) {
        printf("Gagal membuka file!");
        exit(EXIT_FAILURE);
    }

    while (fgets(line, MAX_PATH_LENGTH, filePointer)) {
        if (strstr(line, "ACCESSED") != NULL) {
            access++;
        }
    }

    return access;
    fclose(filePointer);
}
```
Fungsi accessed digunakan untuk menghitung jumlah file yang diakses. 

`filePointer = fopen("log.txt", "r");` fungsi membuka file log.txt dalam mode "r" atau hanya untuk dibaca. 

Kemudian, setiap baris dalam file dibaca menggunakan fgets() dan dicari apakah terdapat kata "ACCESSED" dalam baris tersebut menggunakan strstr(). Jika ditemukan, maka variabel access akan bertambah 1. Fungsi ini mengembalikan nilai variabel access yang berisi jumlah file yang diakses. 

`fclose(filePointer);` file log.txt ditutup menggunakan fclose().

```
void print_dir_total() {
    FILE *filePointer;
    char line[MAX_PATH_LENGTH];

    filePointer = fopen("log.txt", "r");

    if (filePointer == NULL) {
        // kalo kosong exit
        printf("Gagal membuka file!");
        exit(EXIT_FAILURE);
    }

    while (fgets(line, MAX_PATH_LENGTH, filePointer)) {
        line[strlen(line)-1] = '\0';

        if (strstr(line, "MADE") != NULL) {
            char *k = strrchr(line, '/');

            if (k != NULL) {
                dir_total[idxdir] = 0;
                strcpy(dirpath[idxdir++], k+1);
            }
        }

        if (strstr(line, "MOVED") != NULL) {
            char *k = strrchr(line, '/');

            if (k != NULL) {
                for (int i=0 ; i<idxdir ; i++) {
                    if (strcmp(dirpath[i], k+1) == 0) {
                        dir_total[i]++;
                    }
                }
            }
        }
        
    }
    
    for(int i=0;i<idxdir; i++){
        for(int j=0; j<idxdir-1;j++){
            if(dir_total[j] > dir_total[j+1]){
                char buffer[MAX_PATH_LENGTH];
                strcpy(buffer, dirpath[j]);
                strcpy(dirpath[j], dirpath[j+1]);
                strcpy(dirpath[j+1], buffer);

                int temp;
                temp = dir_total[j];
                dir_total[j] = dir_total[j+1];
                dir_total[j+1] = temp;
            }
        }
    }

    for(int i=0; i<idxdir; i++){
        printf("%s : %d\n", dirpath[i], dir_total[i]);
    }

    fclose(filePointer);
}
```
`filePointer = fopen("log.txt", "r");` fungsi membuka file log.txt dalam mode "r" atau hanya untuk dibaca. 

Setelah file berhasil dibuka, fungsi membaca file baris per baris menggunakan fungsi fgets(). Setiap baris yang dibaca kemudian dicari apakah terdapat string "MADE" atau "MOVED" di dalamnya menggunakan fungsi strstr(). 

Jika ditemukan, maka fungsi akan memproses baris tersebut dan mengambil nama direktori terakhir menggunakan fungsi strrchr().

Untuk setiap baris yang mengandung string "MADE", fungsi akan menambahkan nama direktori ke dalam array dirpath[] dan menginisialisasi nilai dir_total[] dengan 0.

Untuk setiap baris yang mengandung string "MOVED", fungsi akan mencari nama direktori pada array dirpath[] yang cocok dengan nama direktori pada baris tersebut. Setiap kali nama direktori ditemukan, maka fungsi akan menambahkan 1 pada nilai dir_total[] pada indeks yang sesuai.

Fungsi juga akan melakukan pengurutan nilai dir_total[] secara ascending menggunakan algoritma bubble sort. Hal ini dilakukan agar output tercetak secara berurutan dari jumlah file yang paling sedikit hingga paling banyak. Setelah itu, fungsi akan mencetak hasil perhitungan ke layar menggunakan fungsi printf().

```
void print_ext_total() {
    FILE *filePointer;
    char line[MAX_PATH_LENGTH];

    filePointer = fopen("extensions.txt", "r");
    if (filePointer == NULL) {
        printf("Gagal membuka file!");
        exit(EXIT_FAILURE);
    }

    while (fgets(line, MAX_PATH_LENGTH, filePointer)) {
        line[strlen(line)-2] = '\0';
        strcpy(extpath[idxext], line);
        ext_total[idxext] = 0;
        idxext++;
    }
    fclose(filePointer);

    strcpy(extpath[idxext], "other");
    ext_total[idxext]=0;
    idxext++;

    filePointer = fopen("log.txt", "r"); 
    if (filePointer == NULL) {
        printf("Gagal membuka file!");
        exit(EXIT_FAILURE);
    }

    while(fgets(line, MAX_PATH_LENGTH, filePointer)){

        if (strstr(line, "MOVED ") != NULL) {
            line[strlen(line)-1] = '\0';
            char *start = strstr(line, "MOVED ");

            char *en = strstr(start, " file :");
            
            strncpy(filename, start + strlen("MOVED "), en - start - strlen("MOVED "));
            filename[en - start - strlen("MOVED ")] = '\0';

            for (int i=0 ; i<idxdir ; i++) {
                if (strcmp(extpath[i], filename) == 0) {
                    ext_total[i]++;
                }
            }
        }


    }
    fclose(filePointer);

    for(int i=0;i<idxext; i++){
        for(int j=0; j<idxext-1;j++){
            if(ext_total[j] > ext_total[j+1]){
                char buffer[MAX_PATH_LENGTH];
                strcpy(buffer, extpath[j]);
                strcpy(extpath[j], extpath[j+1]);
                strcpy(extpath[j+1], buffer);

                int temp;
                temp = ext_total[j];
                ext_total[j] = ext_total[j+1];
                ext_total[j+1] = temp;
            }
        }
    }

    for(int i=0; i<idxext; i++){
        printf("%s : %d\n", extpath[i], ext_total[i]);
    }
}
```
Fungsi membuka file "extensions.txt" yang berisi daftar ekstensi file yang akan dicari jumlah file-nya. Kemudian, fungsi membaca setiap baris pada file tersebut dengan menggunakan perulangan while dan menyimpannya ke dalam variabel line menggunakan fungsi fgets(). 

Variabel extpath pada indeks terakhir diganti menjadi "other" dan ext_total pada indeks tersebut diatur ke 0. Hal ini dilakukan untuk menangani kasus ketika suatu file tidak memiliki ekstensi yang terdaftar pada extensions.txt.

Setelah itu, fungsi membuka file "log.txt" menggunakan fopen() dan membaca setiap baris pada file tersebut dengan menggunakan perulangan while dan menyimpannya ke dalam variabel line. Kemudian, fungsi mencari baris yang mengandung kata "MOVED " pada string line menggunakan strstr(). Jika ditemukan, fungsi memotong string line dari lokasi yang sesuai untuk mendapatkan nama file (tanpa path) yang baru saja dipindahkan, dan kemudian mengecek ekstensi file tersebut dengan membandingkannya dengan setiap nilai di array extpath. Jika terdapat ekstensi file yang cocok, maka variabel ext_total pada indeks yang sesuai akan di increment sebesar 1.

Lalu dilakukan sorting menggunakan bubble sort agar ekstensi dengan jumlah file terbanyak muncul di akhir.
