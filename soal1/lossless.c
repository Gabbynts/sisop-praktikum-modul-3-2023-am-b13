#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

typedef struct pqueueNode_t{
	char huruf;
    int data;
    struct pqueueNode_t *next;
} PQueueNode;

typedef struct pqueue_t{
    PQueueNode *_top;
    unsigned _size;
} PriorityQueue;

void pqueue_init(PriorityQueue *pqueue){
    pqueue->_top = NULL;
    pqueue->_size = 0;
}

bool pqueue_isEmpty(PriorityQueue *pqueue){
    return (pqueue->_top == NULL);
}

void pqueue_push(PriorityQueue *pqueue, int value, char huruf){
    PQueueNode *temp = pqueue->_top;
    PQueueNode *newNode = (PQueueNode*) malloc (sizeof(PQueueNode));
    newNode->huruf = huruf;
    newNode->data = value;
    newNode->next = NULL;

    if (pqueue_isEmpty(pqueue)){
        pqueue->_top = newNode;
        return;
    }

    if (value < pqueue->_top->data){
        newNode->next = pqueue->_top;
        pqueue->_top = newNode;
    }
    else{
        while (temp->next != NULL && temp->next->data < value) temp = temp->next;
        while (temp->next != NULL && temp->next->huruf < huruf){
        	if (temp->next->data > value) break;
			temp = temp->next;
		}
        newNode->next = temp->next;
        temp->next = newNode;
    }
}

void pqueue_pop(PriorityQueue *pqueue){
    if (!pqueue_isEmpty(pqueue)) {
        PQueueNode *temp = pqueue->_top;
        pqueue->_top = pqueue->_top->next;
        free(temp);
    }
}

int pqueue_top(PriorityQueue *pqueue){
    if (!pqueue_isEmpty(pqueue)) return pqueue->_top->data;
    else return 0;
}

void cetak(PriorityQueue *pqueue){
	while (!pqueue_isEmpty(pqueue)) {
        printf("%d %c\n", pqueue->_top->data, pqueue->_top->huruf);
        pqueue_pop(pqueue);
    }
}

typedef struct tree_node{
    char item;
    int freq;
    struct tree_node *left, *right;
} node_tree;

typedef struct tree__{
    node_tree *_root;
} tree__;

void init_huffman(tree__ *tree){
	tree->_root = NULL;
}

bool isEmpty_huffman(tree__ *tree){
	return (tree->_root == NULL);
}

void insert_to_huffman(tree__ *tree, int value, char huruf){
    
    node_tree *newNode = (node_tree*) malloc (sizeof(node_tree));
    newNode->freq = value;
    newNode->item = huruf;
    newNode->left = NULL;
    newNode->right = NULL;
    
    if (isEmpty_huffman(tree)){
    	tree->_root = newNode;
	}
	else{
	    if (newNode->freq < tree->_root->freq){
	    	node_tree *newRoot = (node_tree*) malloc (sizeof(node_tree));
	    	
	    	newRoot->freq = newNode->freq + tree->_root->freq;
    		newRoot->left = newNode;
    		newRoot->right = tree->_root;
    	
    		tree->_root = newRoot;
		}
		else{
			node_tree *newRoot = (node_tree*) malloc (sizeof(node_tree));
    	
    		newRoot->freq = newNode->freq + tree->_root->freq;
    		newRoot->left = tree->_root;
    		newRoot->right = newNode;
    	
    		tree->_root = newRoot;
    	}
	}
}

int main(){
    
    char *filename = "file.txt";
    FILE *file = fopen(filename, "r");

    int frek[100];
    for (int i=0; i<26; i++) frek[i] = 0;
    
    char hrf;
    while ((hrf = fgetc(file)) != EOF){
        if (65<=hrf<=90 || 97<=hrf<=122){
            hrf = toupper(hrf);
            frek[hrf-65]++;
        }
    }
    fclose(file);
    
    PriorityQueue daftar;
    pqueue_init(&daftar);
    
    for (int i=0; i<26; i++){
        printf("%c %d\n", i+65, frek[i]);
        char temp = i+65;
        if (frek[i] > 0) pqueue_push(&daftar, frek[i], temp);
    }
    
    cetak(&daftar);
    
    tree__ huff_tree;
    init_huffman(&huff_tree);
    
    while (!pqueue_isEmpty(&daftar)) {
    	insert_to_huffman(&huff_tree, daftar._top->data, daftar._top->huruf);
        pqueue_pop(&daftar);
    }
    
    pid_t pid;
    pid = fork();
    if (pid == 0){
        // Child process
        
    }
    else{
        // Parent process
        
    }

    return 0;
}